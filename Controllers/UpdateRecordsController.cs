using System.Linq;
using Microsoft.AspNetCore.Mvc;
using NetworthApp.Entities;

namespace NetworthApp.Controllers {

    // Description: Updates context and manipulate records in database
    [Route("api")]
    public class UpdateRecordsController : Controller
    {
        private FormDbContext _context;

        public FormDbContext Context 
        { 
            get 
            {
                return _context;
            }
            set 
            {
                _context = value;
            }
        }
        public UpdateRecordsController(FormDbContext context) 
        {
            Context = context;
        }

        // Retrieve records
        [Route("GetData")]
        public IActionResult GetRecords() 
        {
            try 
            {
                return Ok(Context.Records);
            }
            catch (System.Exception e) 
            {
                System.Console.WriteLine(e.ToString());
                return Ok(null);
            }
        }

        // Add new entry to database
        [Route("PostData/{input}")]
        public string PostData(string input) 
        {
            string[] split = input.Split(new char[] { ' ' });
            bool validInput = true;
            var type = "";
            var name = "";
            double balance = 0;
            
            if (!(split.Length == 3) || 
                !(split[0] == "Asset" || split[0] == "Liability"))
            {
                validInput = false;
            }

            try 
            {
                balance = System.Convert.ToDouble(split[2]);
            }
            catch 
            {
                validInput = false;
            }
            
            if (!validInput) 
            {
                return "invalid input";
            }

            type = split[0];
            name = split[1];

            // Query the database and see if there are any matching names
            try 
            {
                if (Context.Records.Any(record => record.Name == name)) {
                    // Don't add matching names
                    return "invalid input";
                }
            }
            catch (System.Exception e) 
            {
                System.Console.WriteLine(e.ToString());
                return "unable to access database";
            }
            
            // Add new record
            var newRecord = new Record() {
                Type = type,
                Name = name,
                Balance = balance
            };

            try 
            {
                Context.AddAsync(newRecord);
                Context.SaveChanges();
            }
            catch (System.Exception e) 
            {
                System.Console.WriteLine(e.ToString());
                return "unable to access database";
            }
                
            return "" + newRecord.Type + " " + newRecord.Name + " " + newRecord.Balance;
        }
        
        // Deletes an entry from the database
        // @nameField: optionally delete an entry based on name
        // @indexField: optionally delete an entry based on index
        [Route("DeleteData/{nameField}/{indexField}")]
        public string DeleteData(string nameField, string indexField) 
        {
            int index = -1;
            if (indexField != "na") 
            {
                try 
                {
                    index = System.Convert.ToInt16(indexField);
                }
                catch 
                {
                    return "invalid input";
                }
            }

            if (nameField != "na") 
            {
                // Remove record with name nameField
                try 
                {
                    try 
                    {
                        var nameEntry = Context.Records.Where(record => record.Name == nameField).First();
                        if (nameEntry != null)
                        {
                            Context.Records.Remove(nameEntry);
                            Context.SaveChanges();
                        }
                    }
                    catch (System.Exception e) 
                    {
                        System.Console.WriteLine(e.ToString());
                        return "unable to access database";
                    }
                }
                catch
                {
                    return "invalid input";
                }
                return "done";
            }

            else if (indexField != "na") 
            {
                // Get records with index indexField
                if (index >= 0 && index < Context.Records.Count()) {
                    try 
                    {
                        var indexEntry = Context.Records.ToList()[index];
                        Context.Records.Remove(indexEntry);
                        Context.SaveChanges();
                    }
                    catch (System.Exception e) 
                    {
                        System.Console.WriteLine(e.ToString());
                        return "unable to access database";
                    }
                }
                return "done";
            }
            
            else {
                return "invalid input";
            }
        }
    }
}