namespace NetworthApp.Enums 
{
    enum Type {
        
        // Adds to total balance
        Asset,
        
        // Subtracts from total balance
        Liability
    }
}